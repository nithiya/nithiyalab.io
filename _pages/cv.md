---
title: CV
excerpt: ""
permalink: /cv/
last_modified_at: 2024-11-19
toc: true
toc_sticky: true
---

## Experience

**Scientific Officer II** \
[**Environmental Protection Agency Ireland**](https://www.epa.ie/) \| Wexford, IE \
Nov 2024 --- Present

**Research Assistant** \
[**University College Cork**](https://www.ucc.ie/en/) \| Cork, IE \
May 2023 --- Nov 2024 \
*Project:* H-Wind -- Hydrogen from Offshore Wind \
*Funding:* Science Foundation Ireland, ESB, DP Energy, Gas Networks Ireland, and Equinor \
May 2022 --- May 2023 \
*Project:* ClimAg -- Multifactorial causes of fodder crises in Ireland and risks due to climate change \
*Funding:* Environmental Protection Agency -- Climate Change Research Programme; Higher Education Authority -- COVID-19 research support scheme

**Research Fellow** \
[**University of Stavanger**](https://www.uis.no/en) \| Stavanger, NO \
Apr 2018 --- Apr 2020 \
*Project:* ENSYSTRA -- ENergy SYStems in TRAnsition in the North Sea Region \
*Funding:* European Union -- Horizon 2020 Research and Innovation Programme -- Marie Skłodowska‐Curie Actions

## Education

[**University of Aberdeen**](https://www.abdn.ac.uk/) \| Aberdeen, UK \
2021 --- 2022 \
**MSc in Geographical Information Systems** \
*Dissertation in collaboration with Community Energy Scotland:* "Mapping the impacts of a proposed offshore wind development plan on Isle of Lewis communities"

[**Heriot-Watt University**](https://www.hw.ac.uk/) \| Edinburgh, UK \
2016 --- 2017 \
**MSc in Renewable Energy Engineering** \
*Industry-based dissertation in collaboration with Natural Power:* "Specification of 'normal' wind turbine operating behaviour for rapid anomaly detection: through the use of machine learning algorithms" \
2013 --- 2016 \
**BEng in Mechanical & Energy Engineering**

## Technical Skills

- **Programming & data analysis:** Python, R, GitHub, GitLab, JupyterLab, SQL, Microsoft Excel
- **Geospatial software:** ArcGIS products, QGIS, CDO
- **Typesetting & documentation:** LaTeX, Microsoft Office suite, HTML, CSS, JavaScript, Sphinx
- **Operating Systems:** Linux, Microsoft Windows, Windows Subsystem for Linux

## Language Skills

- **English:** Fluent
- **Malay:** Fluent
- **Tamil:** Intermediate
- **Norwegian (Bokmål):** Basic; completed the A1 Norwegian Course for Foreigners with Higher Education (Norsk for voksne innvandrere) in Stavanger in 2019
- **Irish:** Basic; completed the Beginners' Course in Spoken Irish (Cúrsa do Thosnaitheoirí sa Ghaeilge Labhartha) in Cork in 2023

## Training

- Research Skills Training Programme (Continuing Professional Development) -- University College Cork, IE (2023)
- Cartography. -- Esri, Online (2022)
- Career Mentoring Programme -- University of Aberdeen, UK (2021)
- Energy Law, Policy, Planning & Governance -- University of Groningen, NL (2019)
- Mathematics for Machine Learning Specialisation -- Imperial College London on Coursera, Online (2019)
- Energy Economics: Markets, Investment & Business -- University of Edinburgh, UK (2019)
- Modelling Energy Systems: Setting the Interdisciplinary Stage -- Europa‐Universität Flensburg, DE (2018)
- Introduction to Energy Systems in Transition in the North Sea Region -- University of Groningen, NL (2018)
- Blue Growth Summer School -- Ghent University, BE (2017)

## Activities

- **Reviewer** -- Journal of Open Source Software (JOSS)
- **eBirder** -- Cornell Lab of Ornithology
