---
title: Converting Jupyter notebooks to Python scripts
excerpt: ""
tags:
  - python
  - jupyter
last_modified_at: 2024-09-24
toc: false
---

Here's a little shell script I use to convert multiple [Jupyter](https://jupyter.org/) notebooks into formatted Python scripts:

```sh
# sanitise the HTML notebooks (not needed often)
jupyter nbconvert --sanitize-html --to notebook --inplace */*.ipynb

# convert Jupyter Notebooks to Python scripts
jupyter nbconvert --to script --ClearOutputPreprocessor.enabled=True */*.ipynb

# format scripts and notebooks (+ max 79 characters per line)
black -l 79 **/*.py **/*.ipynb

# sort imports
isort **/*.py

# move files to scripts directory
mkdir -p scripts
mv */*.py scripts
```

This script assumes that the Jupyter notebooks are stored in a sub-directory, e.g. `notebooks`. The formatted Python scripts are then moved to another sub-directory, i.e. `scripts`. [Black](https://black.readthedocs.io/en/stable/) is used to format the scripts and notebooks and [isort](https://pycqa.github.io/isort/) is used to sort the imports, while the conversion itself is performed using [nbconvert](https://nbconvert.readthedocs.io/en/latest/).

If anyone has a better solution to this, I'd love to hear it!
