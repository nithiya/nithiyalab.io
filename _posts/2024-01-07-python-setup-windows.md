---
title: Python setup guide for Windows
excerpt: A quick guide for my colleagues
tags:
  - windows
  - vscode
  - wsl
  - conda
last_modified_at: 2024-08-19
---

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Related** \
See also my post on <a href="{{ "post/development-environment" | relative_url }}">setting up a Git development environment</a> and <a href="{{ "post/r-setup" | relative_url }}">R</a> on Windows.

## Windows without WSL

1.  Check if you have [Windows Terminal](https://learn.microsoft.com/en-us/windows/terminal/) installed. If not, [install it from the Microsoft Store](https://aka.ms/terminal).

1.  Launch a Windows Terminal instance. This should start a PowerShell session.

1.  Update the PowerShell [execution policy](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies) by running the following command:

    ```powershell
    Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned
    ```

1.  Run `Get-ExecutionPolicy` to check if it is set to `RemoteSigned`. If not, try setting it again in a terminal with administrative rights.

1.  Check if winget (the Windows package manager) is installed. The following command should bring up details about winget. If not, [install it from the Microsoft Store](https://learn.microsoft.com/en-us/windows/package-manager/winget/).

    ```powershell
    winget --help
    ```

    {: .notice--info}
    {% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
    When using winget for the first time, you will be asked to accept some terms and conditions.

1.  Install a text editor. We will use [Visual Studio Code](https://code.visualstudio.com/).

    ```powershell
    winget install vscode
    ```

1.  Find an appropriate Python version to install using the following command.

    ```powershell
    winget search --tag python --cmd python --source winget
    ```

    This will output the following:

    ```text
    Name            Id                  Version     Match
    -----------------------------------------------------------
    Python 3        Python.Python.3.9   3.9.13      Tag: python
    Python 3.12     Python.Python.3.12  3.12.0      Tag: python
    Python 3.11     Python.Python.3.11  3.11.6      Tag: python
    Python 3.10     Python.Python.3.10  3.10.11     Tag: python
    Python 2        Python.Python.2     2.7.18150   Tag: python
    Python Launcher Python.Launcher     3.12.0      Tag: python
    Miniconda3      Anaconda.Miniconda3 py39_4.10.3 Tag: python
    Anaconda3       Anaconda.Anaconda3  2023.09     Tag: python
    ```

1.  Install [Python](https://www.python.org/). At the time of writing this guide, the latest version was 3.12, so I am installing 3.11.

    ```powershell
    winget install python.python.3.11
    ```

1.  Check if Python was installed correctly. This command should display the version.

    ```powershell
    py --version
    ```

1.  Launch Visual Studio Code and install the following extensions:

    - [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
    - [Jupyter](https://marketplace.visualstudio.com/items?itemName=ms-toolsai.jupyter)

1.  Create a project folder, e.g. in `C:\User\Username\Documents\project_folder`.

1.  [Create a virtual environment](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/) in the project folder.

    ```powershell
    cd C:\User\Username\Documents\project_folder
    py -m venv .venv
    .venv\Scripts\activate
    ```

1.  Install appropriate packages (e.g. [JupyterLab](https://jupyter.org/)) in the virtual environment.

    ```powershell
    py -m pip install --upgrade pip setuptools wheel
    py -m pip install jupyterlab matplotlib pandas
    ```

1.  Create an example Jupyter Notebook in the project folder, e.g. `my_notebook.ipynb`.

1.  Try to open the notebook using either VS Code or JupyterLab. To launch JupyterLab, run the following in the terminal from the project folder, which will launch a web browser instance:

    ```powershell
    jupyter lab
    ```

1.  Activate the kernel in the Jupyter notebook and start coding!

## Windows Subsystem for Linux (WSL) - Ubuntu

1.  In PowerShell, install Visual Studio Code and [WSL](https://learn.microsoft.com/en-us/windows/wsl/) (Ubuntu):

    ```powershell
    winget install vscode
    wsl --install
    ```

1.  Install the [WSL VS Code extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl) locally in Windows.

1.  Launch an Ubuntu Bash shell instance from Windows Terminal.

1.  Python should already be installed; check its version:

    ```sh
    python --version
    ```

    {: .notice--info}
    {% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
    Note that it's `python` on Ubuntu, not `py`.

1.  Launch VS Code using the WSL shell from the project folder's directory:

    ```sh
    cd C:/User/Username/Documents/project_folder
    code .
    ```

1.  Install local VS Code extensions (e.g. Python and Jupyter) in WSL.

1.  Set-up a virtual environment in the project folder and install relevant packages:

    ```sh
    python3 -m venv .venv
    source .venv/bin/activate
    python -m pip install --upgrade pip setuptools wheel
    python -m pip install jupyterlab matplotlib pandas
    ```

## Further reading

- <https://code.visualstudio.com/docs/remote/wsl>
