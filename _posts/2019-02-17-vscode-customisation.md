---
title: My Visual Studio Code customisations
excerpt: ""
tags:
  - vscode
  - git
  - wsl
  - windows
  - linux
last_modified_at: 2024-03-19
toc: false
---

[Visual Studio Code](https://code.visualstudio.com/) is a versatile cross-platform text editor. It has been developed by the VS Code team at Microsoft and the [source code](https://github.com/Microsoft/vscode) is distributed under the terms of the [MIT License](https://opensource.org/license/mit).

It's worth noting that their official binaries are not fully open-source, however, as they come bundled with Microsoft branding, telemetry and licensing and are released under the [Microsoft Software License Terms](https://code.visualstudio.com/License/). To avoid this, you're free to compile it yourself using the source code, or you can download [VSCodium](https://vscodium.com/) instead. I use VSCodium myself and can highly recommend it.

There are a large number of useful extensions for VS Code that you can find in the [Visual Studio Marketplace](https://marketplace.visualstudio.com/) or, alternatively, the [Open VSX Registry](https://open-vsx.org/). I'm listing the ones I use here. Most of these are highly configurable, so you can customise them to suit your needs.

- [Code Spell Checker](https://github.com/streetsidesoftware/vscode-spell-checker)

- [LaTeX Workshop](https://github.com/James-Yu/LaTeX-Workshop)

- [markdownlint](https://github.com/DavidAnson/vscode-markdownlint) and [Markdown All in One](https://github.com/yzhang-gh/vscode-markdown)

- [Python](https://github.com/Microsoft/vscode-python), [Jupyter](https://github.com/Microsoft/vscode-jupyter), [isort](https://github.com/microsoft/vscode-isort), [Flake8](https://marketplace.visualstudio.com/items?itemName=ms-python.flake8), and [Pylint](https://marketplace.visualstudio.com/items?itemName=ms-python.pylint)

- [Rainbow CSV](https://github.com/mechatroner/vscode_rainbow_csv)

- [R LSP Client](https://github.com/REditorSupport/vscode-r-lsp)

- [EditorConfig](https://github.com/editorconfig/editorconfig-vscode)

- [HTML-validate](https://marketplace.visualstudio.com/items?itemName=html-validate.vscode-html-validate)

To develop in [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/), VS Code for Windows must be installed along with the [WSL Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl) extension. [Read the docs](https://code.visualstudio.com/docs/remote/wsl) for more information.
