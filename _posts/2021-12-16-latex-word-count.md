---
title: LaTeX word count automation
excerpt: ""
tags:
  - latex
  - typesetting
last_modified_at: 2021-12-17
toc: false
g1:
  - url: /assets/images/latex-word-count/output.png
    image_path: /assets/images/latex-word-count/output.png
    alt: "PDF output."
    title: "PDF output."
---

The number of words in a [LaTeX](https://www.latex-project.org/) document can be calculated using the [TeXcount](https://app.uio.no/ifi/texcount/) utility, which is available on [CTAN](https://ctan.org/pkg/texcount) and is part of the [TeX Live](https://tug.org/texlive/) distribution. This post demonstrates how to automatically include the word count in the PDF document during compilation.

Consider the following LaTeX file, with the file path `doc/main.tex`:

{% highlight latex linenos %}
\documentclass{article}

\title{Document title}

\begin{document}

\maketitle

\section{Section title}

Sample text. Sample text. Sample text. Sample text.

%TC:ignore
% insert word count at the bottom right
\vfill\hfill Word count: \input{wordcount}%
%TC:endignore

\end{document}
{% endhighlight %}

Run the following command to switch to the file's directory, get the word count, write it to a file with the path `doc/wordcount.tex`, and compile the PDF:

```sh
cd doc && texcount -merge -template='{1}' main.tex -out='wordcount.tex' && xelatex main.tex
```

In the `texcount` command, `-merge` ensures that all included files are merged into the document during the count, and `-template='{1}'` uses the output template that counts only the main body text. See TeXcount's website or CTAN documentation for more information about the command-line options available.

This is the resulting document:

{% include gallery id="g1" caption="PDF output." %}

All text enclosed within `%TC:ignore` and `%TC:endignore` will be omitted from the word count[^1].

To add thousands separators[^2] [^3] to the word count, use the following command instead:

```sh
cd doc && printf "%'d" $(texcount -merge -template='{1}' main.tex) > wordcount.tex && xelatex main.tex
```

## Footnotes

[^1]: [LaTeX Stack Exchange - Make TeXcount ignore appendices](https://tex.stackexchange.com/a/259296)
[^2]: [Unix & Linux Stack Exchange - Add thousands separator in a number](https://unix.stackexchange.com/a/113926)
[^3]: [Unix & Linux Stack Exchange - How to pass the output of one command as the command-line argument to another?](https://unix.stackexchange.com/q/4782)
