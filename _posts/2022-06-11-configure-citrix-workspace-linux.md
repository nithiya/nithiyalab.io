---
title: Configuring Citrix Workspace on Arch Linux
excerpt: ""
tags:
  - linux
  - citrix
  - virtual desktop
last_modified_at: 2023-04-09
toc: false
---

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
The instructions below concern setting up Citrix Workspace manually on Arch Linux. Alternatively, one can install Citrix through the [AUR](https://aur.archlinux.org/packages/icaclient).

Download the Workspace app for Linux from the [Citrix download page](https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html).

Install some prerequisite packages to ensure the GUI works properly[^4]:

```sh
sudo pacman -Syu webkit2gtk gtk2
```

Assuming the Workspace app archive was downloaded to `~/Downloads`, extract the archive and proceed with the installation as follows[^1]:

```sh
mkdir ~/Downloads/Citrix
mv linuxx*.tar.gz ~/Downloads/Citrix/
cd ~/Downloads/Citrix
tar xzvf linuxx*.tar.gz
./setupwfc
```

Set the `ICAROOT` environment variable in `~/.zshrc` or equivalent[^1]:

```sh
echo '
# Citrix Workspace - ICAClient
export ICAROOT=~/ICAClient/linuxx64/' >> ~/.zshrc
source ~/.zshrc
```

Then, configure the certificates (this may be system-dependent)[^2] [^3]:

```sh
mkdir -p $ICAROOT/keystore/cacerts/
cd $ICAROOT/keystore/cacerts/
cp /etc/ca-certificates/extracted/tls-ca-bundle.pem .
awk 'BEGIN {c=0;} /BEGIN CERT/{c++} { print > "cert." c ".pem"}' < tls-ca-bundle.pem
openssl rehash $ICAROOT/keystore/cacerts/
$ICAROOT/util/ctx_rehash
```

The configuration is complete and virtual desktops can now be launched by opening ICA files with Citrix Workspace Engine.

The installation files can be safely deleted at this point:

```sh
rm -r ~/Downloads/Citrix
```

To reinstall or uninstall the application[^1], run the setup script as follows:

```sh
$ICAROOT/setupwfc
```

## Footnotes

[^1]: [Install, Uninstall, and Update - Citrix Workspace app for Linux - Product documentation](https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/install.html)
[^2]: [TLS/SSL Certificates - Citrix - ArchWiki](https://wiki.archlinux.org/title/Citrix#TLS/SSL_Certificates)
[^3]: [Citrix receiver ssl connection couldn't be established - Ask Ubuntu](https://askubuntu.com/a/830129)
[^4]: [System requirements and compatibility - Citrix Workspace app for Linux - Product documentation](https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/system-requirements.html)
