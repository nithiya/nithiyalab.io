---
title: Installing a custom ROM on Samsung Galaxy Tab 2
excerpt: ""
tags:
  - android
  - samsung
last_modified_at: 2023-11-15
g1:
  - url: /assets/images/custom-rom-android-espresso3g/download-mode.jpg
    image_path: /assets/images/custom-rom-android-espresso3g/download-mode.jpg
    alt: "A Samsung device in download mode. Source: Cosas De Android; License: CC-BY-NC-4.0."
    title: "A Samsung device in download mode. Source: Cosas De Android; License: CC-BY-NC-4.0."
comments: true
---

These are the steps I used to restore an old [Samsung Galaxy Tab 2 10.1](https://en.wikipedia.org/wiki/Samsung_Galaxy_Tab_2_10.1) GSM (P5100)[^1] purchased in 2012, which came with Android Ice Cream Sandwich (4.0.3) and was upgraded to Jelly Bean (4.1.2)[^1]. There were issues installing and updating apps from the Google Play Store. I attempted to fix the issue and ended up installing a custom recovery image and ROM with a more recent Android version (i.e. replacing the stock Android operating system).

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Tip** \
As always, step zero is to backup all important files!

## Finding the right custom recovery and ROM

Following guides by [Android-Andi](https://andi34.github.io) and on [XDA Developers](https://xdaforums.com/) forum threads, I decided to go with the following:

- **Recovery:** [TWRP for Samsung Galaxy Tab 2 (GSM - unified)](https://twrp.me/samsung/samsunggalaxytab2gmsunified.html)
- **ROM:** [OmniROM](https://omnirom.org/) 6.0 - [unofficial unified build](https://andi34.github.io/roms_tab2_omni.html) with Android 6 (Marshmallow)

The unified versions (codename: espresso3g) work on both P5100 and P3100. I'm using the unified version of TWRP as while a device-specific version for P5100 exists, it has been discontinued[^5]. Although an OmniROM build with Android 7 exists, I went with the Android 6 build, as the former has unresolved bugs[^6] while the latter is more stable. Moreover, I went with the unofficial build of OmniROM, as the official builds have been discontinued[^7].

Since OmniROMs are free of Google Play services[^15], and my sister needs Google apps to work, I also downloaded [Open GApps](https://opengapps.org/). I downloaded the pico variant (since I only want a minimal install of Google Play) for ARM platforms (since the tab has 32-bit architecture) and Android 6.0 (same as the ROM of my choice).

After downloading the ZIP files for OmniROM and Open GApps, I copied them to an external microSD card mounted on the tab[^8].

## Setting up tools to flash a custom recovery

I had to unlock developer mode (by tapping the build version in the settings a number of times), and then enable USB debugging.

I also needed to install a programme on my computer in order to flash the custom recovery on the tab via a USB cable connection. For many devices, this is achieved by unlocking the bootloader of the Android device and running Fastboot, which is part of the [Android SDK tools](https://developer.android.com/studio)[^3] [^4]. However, this is not supported on Samsung devices, so I needed to use either Heimdall or Odin[^2]. I went with Heimdall; [Heimdall's homepage](https://glassechidna.com.au/heimdall/) sums up nicely why Odin is not recommended:

> For internal use, Samsung developed their own firmware flashing tool known as 'Odin' [...]
>
> Aside from being slow and generally unreliable, Odin only runs on Windows systems. Furthermore, Odin is 'leaked software' that is not officially supported by Samsung, freely available, or well understood by the community at large.

Heimdall is an open-source (MIT License), cross-platform tool; its source code is available on [GitHub](https://github.com/Benjamin-Dobell/Heimdall). The repository has operating-system-specific README files on how to build the programme from source. These are the steps I followed on my Windows 10 system[^10]:

- Download and install [MSYS2](https://www.msys2.org/) ([detailed installation instructions](https://www.msys2.org/wiki/MSYS2-installation/)), which serves as the [MinGW-w64](https://en.wikipedia.org/wiki/Mingw-w64) build environment.
- In the command prompt that launches, run the following to install all requirements:

  ```sh
  Pacman -S mingw-w64-x86_64 mingw-w64-x86_64-clang mingw-w64-x86_64-cmake mingw-w64-x86_64-libusb mingw-w64-x86_64-qt5-static make
  ```

- Add MinGW-w64 to the PATH environment variable:

  ```sh
  export PATH="/mingw64/bin:$PATH"
  ```

- Set up MSYS2 to be used on Windows Terminal[^9], and launch a Windows Terminal window with the `MINGW / MSYS2` shell.

- Clone the Heimdall GitHub repository.

  via HTTPS:

  ```sh
  git clone https://github.com/Benjamin-Dobell/Heimdall.git
  ```

  via SSH:

  ```sh
  git clone git@github.com:Benjamin-Dobell/Heimdall.git
  ```

- Run the following to build Heimdall:

  ```sh
  cd Heimdall
  mkdir build
  cd build
  cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DQt5Widgets_DIR=/c/msys64/mingw64/qt5-static/lib/cmake/Qt5Widgets ..
  make
  ```

## Flashing the custom recovery image

I turned off the tab and entered download mode. I did this by pressing the Volume Up and Power buttons for a few seconds until a yellow warning triangle appears on the screen. I then pressed the Volume Down button to proceed into ODIN mode.

I then connected the tab to my computer using a USB cable and followed these instructions[^10]:

- Execute `zadig.exe`, located in `Heimdall/Win32/Drivers`, to install (or replace) the Samsung USB driver[^12].
- Open a `MINGW64 / MSYS2` terminal window in the path where Heimdall was built (`Heimdall/build/bin`).
- Run `./heimdall help` to list all of Heimdall's functionality.
- Run `./heimdall detect` to verify whether the tab is connected via USB.
- Run the following to flash the custom recovery, replacing `recovery.img` with the full path of the downloaded TWRP recovery image[^13]:

  ```sh
  ./heimdall flash --RECOVERY recovery.img --no-reboot
  ```

- Once the flash is complete, disconnect the USB cable and reboot into recovery mode by pressing the Volume Down and Power buttons for a few seconds to access TWRP.

  {: .notice--warning}
  {% include fontawesome class="fa-solid fa-circle-exclamation" %} **Warning** \
  *Do not* boot into the Android system, as this can cause the stock recovery to replace the newly-flashed TWRP[^4] [^11].

## Creating backups and flashing the custom ROM

Before flashing a custom ROM, I created a backup of the stock Android system of the tab, so that I can go back to it in case something goes wrong. I tapped "Backup" on TWRP and selected the System, Data, and Boot partitions, which I backed up to the external microSD card[^8] [^14] [^16].

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
Read more about backing up and formatting Android devices on the [TWRP FAQ](https://twrp.me/FAQ/).

I then followed these instructions[^8] [^16], which include formatting the internal storage and installing OmniROM and Open GApps:

- Go to TWRP's home and tap "Wipe", tap "Format Data", type "yes", and tap the tick mark to proceed.
- Go back to "Wipe" and tap "Advanced Wipe", select "Dalvic / ART Cache", "System", and "Cache", and swipe to proceed.
- Go back to TWRP's home and tap "Reboot", tap "Recovery", and swipe to reboot.
- After rebooting, tap "Install", navigate to the external microSD card directory, select the OmniROM ZIP file, and flash it.
- Go back to "Install" and flash Open GApps the same way.
- Go back to TWRP's home and tap "Reboot" to reboot into the new Android system.

That's it!

## Footnotes

[^1]: [Samsung Galaxy Tab 2 10.1 P5100 - GSMArena](https://www.gsmarena.com/samsung_galaxy_tab_2_10_1_p5100-4567.php)
[^2]: [Fastboot on Samsung devices - Android Enthusiasts Stack Exchange](https://android.stackexchange.com/a/162667)
[^3]: [What is Fastboot? - Android Central](https://www.androidcentral.com/android-z-what-fastboot)
[^4]: [FASTBOOT commands don't work at all on my Samsung phone - Android Enthusiasts Stack Exchange](https://android.stackexchange.com/a/187573)
[^5]: [Recoveries for Samsung Galaxy Tab 2 - OpenSource Development by Android-Andi @ XDA](https://andi34.github.io/recoveries_tab2.html)
[^6]: [[ROM][7.x][unified] OmniROM - XDA Developers Forums](https://xdaforums.com/t/rom-7-x-unified-omnirom.3661600/)
[^7]: [OmniRoms for Samsung Galaxy Tab 2 - OpenSource Development by Android-Andi @ XDA](https://andi34.github.io/roms_tab2_omni.html)
[^8]: [FAQ - Frequently Asked Questions - OpenSource Development by Android-Andi @ XDA](https://andi34.github.io/faq.html)
[^9]: [Windows Terminal - Terminals - MSYS2](https://www.msys2.org/docs/terminals/#windows-terminal)
[^10]: [Win32 - Benjamin-Dobell/Heimdall - GitHub](https://raw.githubusercontent.com/Benjamin-Dobell/Heimdall/master/Win32/README.txt)
[^11]: [TWRP for Samsung Galaxy Tab 2 (GSM - unified) - TWRP](https://twrp.me/samsung/samsunggalaxytab2gmsunified.html)
[^12]: [Zadig - pbatard/libwdi Wiki - GitHub](https://github.com/pbatard/libwdi/wiki/Zadig)
[^13]: [Heimdall errors, "ERROR: Partition "recovery" does not exist in the specified PIT." - Android Enthusiasts Stack Exchange](https://android.stackexchange.com/a/51066)
[^14]: [What should I back up in TWRP? - TWRP](https://twrp.me/faq/whattobackup.html)
[^15]: Installing Omni on your device - Omni Docs
[^16]: [How to wipe in TWRP properly? - OpenSource Development by Android-Andi @ XDA](https://andi34.github.io/faq/faq_twrp.html)
