---
title: Setting up development environments on Windows and Linux
excerpt: Developing with Git
tags:
  - ssh
  - windows
  - git
  - vscode
  - linux
  - wsl
  - conda
  - github
last_modified_at: 2024-05-05
---

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Related** \
See also my post on <a href="{{ "post/python-setup-windows" | relative_url }}">setting up Python on Windows</a> as well as <a href="{{ "post/r-setup" | relative_url }}">R</a>.

## Prerequisites

Install a [source code editor](https://en.wikipedia.org/wiki/Source-code_editor). We will use [Visual Studio Code](https://code.visualstudio.com/) in this post.

### Windows users

Check if you have [Windows Terminal](https://learn.microsoft.com/en-us/windows/terminal/) installed.
If not, [install it from the Microsoft Store](https://aka.ms/terminal).

Launch a Windows Terminal instance.
This should start a PowerShell session.

Update the PowerShell [execution policy](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies) by running the following command:

```powershell
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy RemoteSigned
```

Run `Get-ExecutionPolicy` to check if it is set to `RemoteSigned`.
If not, try setting it again in a terminal with administrative rights.

Check if [winget](https://learn.microsoft.com/en-us/windows/package-manager/winget/) (the Windows package manager) is installed. The following command should bring up details about winget. If not, [install it from the Microsoft Store](https://learn.microsoft.com/en-us/windows/package-manager/winget/).

```powershell
winget --help
```

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
When using winget for the first time, you will be asked to accept some terms and conditions.

#### Native Windows

Install [Git](https://git-scm.com/) using winget:

```powershell
winget install git.git
```

Install these additional packages if you regularly use [GitHub](https://github.com/):

```powershell
winget install github.githubdesktop
winget install github.cli
winget install github.gitlfs
```

#### Windows with Windows Subsystem for Linux (WSL) - Ubuntu

Install WSL (you may have to reboot after the installation):

```sh
wsl --install
```

Launch VS Code and install the WSL and Remote - SSH extensions.

## Configuration

Configure [Git global user settings](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup):

```sh
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
```

### HTTPS

Authenticate [VS Code](https://code.visualstudio.com/docs/sourcecontrol/overview) using HTTPS.

Also authenticate your GitHub account on [GitHub Desktop](https://docs.github.com/en/desktop/installing-and-authenticating-to-github-desktop/authenticating-to-github-in-github-desktop) and [GitHub CLI](https://cli.github.com/manual/index) if you installed them.

{: .notice--success}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
That's all you need to do to get Git working over HTTPS (easy!).
Read further for setting up SSH authentication, which is more complicated.

### SSH

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
Use Git Bash for running the following commands if you are using native Windows.
If you are using WSL, use the Ubuntu Bash shell instead.

Check for existing SSH keys:

```sh
ls -al ~/.ssh
```

If no keys exist, generate a new key **[using ED25519 (recommended over RSA)](https://docs.gitlab.com/ee/user/ssh.html):**

```sh
ssh-keygen -t ed25519 -C "<comment>"
```

Start the SSH agent as follows (you may also need add these to `~/.bashrc` or equivalent if it doesn't activate automatically):

```sh
eval $(ssh-agent)
ssh-add
```

Copy the SSH public key to your clipboard (saved at `~/.ssh/id_ed25519.pub`) and add to GitHub/GitLab. The key can be used for authentication, signing, or both.

If the key will be used for signing as well, configure the following:

```sh
git config --global commit.gpgsign true
git config --global gpg.format ssh
git config --global user.signingkey ~/.ssh/id_ed25519.pub
```

In VS Code, enable commit signing in the settings:

```json
"git.enableCommitSigning": true
```

## Troubleshooting SSH issues

### Native Windows

If the above configurations did not work, try using [OpenSSH for Windows](https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse) instead of the SSH that comes with Git.

[Set OpenSSH to run automatically](https://github.com/Microsoft/vscode/issues/13680#issuecomment-414841885). Go to Task Manager > Services > Open Services, find OpenSSH Authentication Agent > Properties > Startup Type > Automatic.

Configure Git to use OpenSSH (in Git Bash):

```sh
git config --global core.sshCommand "C:/Windows/System32/OpenSSH/ssh.exe"
eval $(ssh-agent)
ssh-add
```

To revert to the SSH agent supplied by Git for Windows (in Git Bash):

```sh
git config --global core.sshCommand "C:/Program Files/Git/usr/bin/ssh.exe"
eval $(ssh-agent)
ssh-add
```

### Linux and WSL

If there are SSH issues on Ubuntu, try using [Keychain](https://www.funtoo.org/Funtoo:Keychain).

If there are SSH issues on KDE, see [here](https://forum.manjaro.org/t/howto-use-kwallet-as-a-login-keychain-for-storing-ssh-key-passphrases-on-kde/7088) and [here](https://forum.manjaro.org/t/vscode-git-ssh-askpass-exec-usr-lib-ssh-ssh-askpass-no-such-file-or-directory/78787).

## VS Code with WSL

Install packages such as Miniconda using the WSL Bash shell (use the Unix shell script to install).

Clone Git repositories using the WSL Bash shell using SSH.

Launch VS Code using the WSL Bash shell by navigating to the Git repository directory and running `code .`.

Install local VS Code extensions in WSL.

## Testing if SSH works

Use the following commands to test whether SSH has been configured properly for GitLab, GitHub, and GitHub Gist:

```sh
ssh -T git@gitlab.com  # GitLab
ssh -T git@github.com  # GitHub
```

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
If connection to these hosts are being tested for the first time, a warning might appear stating that the authenticity of the host can't be established and if you want to continue connecting. If the fingerprint matches that of [GitLab](https://docs.gitlab.com/ee/user/gitlab_com/index.html#ssh-host-keys-fingerprints) or [GitHub](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints), type "yes" and continue.

You should get the following outputs if it's working:

```text
Welcome to GitLab, @username!

Hi username! You've successfully authenticated, but GitHub does not
provide shell access.
```

If you don't get the above, then there may be issues with the connection. To troubleshoot, use the following command to run SSH in verbose mode:

```sh
ssh -Tvvv git@gitlab.com  # GitLab
ssh -Tvvv git@github.com  # GitHub
```

[Enabling SSH via HTTPS](https://docs.github.com/en/authentication/troubleshooting-ssh/using-ssh-over-the-https-port) might resolve this problem.

First, test if SSH is working over the HTTPS port (443); you should get the same outputs as above:

```sh
ssh -T -p 443 git@altssh.gitlab.com  # GitLab
ssh -T -p 443 git@ssh.github.com  # GitHub
```

If it works, create a file called `~/.ssh/config` and add the following configurations:

```sh
Host gitlab.com
  Hostname altssh.gitlab.com
  User git
  Port 443
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ed25519

Host github.com
  Hostname ssh.github.com
  User git
  Port 443
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ed25519

Host gist.github.com
  Hostname ssh.github.com
  User git
  Port 443
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ed25519
```

Test again to see if everything is working:

```sh
ssh -T git@gitlab.com  # GitLab
ssh -T git@github.com  # GitHub
ssh -T git@gist.github.com  # GitHub Gist
```

## Updating remote URLs for SSH

If the local repository was cloned using HTTPS and you're using SSH, the remote URL must be updated to use SSH. To check the existing remote:

```sh
git remote -v
```

To change the remote URL to SSH:

```sh
git remote set-url origin git@gitlab.com:USERNAME/REPOSITORY.git  # GitLab
git remote set-url origin git@github.com:USERNAME/REPOSITORY.git  # GitHub
```

## Links

- <https://learn.microsoft.com/en-us/windows/wsl/>
- <https://code.visualstudio.com/docs/remote/wsl>
- <https://docs.gitlab.com/ee/user/ssh.html>
- <https://support.atlassian.com/bitbucket-cloud/docs/set-up-personal-ssh-keys-on-windows/>
- <https://docs.github.com/en/authentication/connecting-to-github-with-ssh>
- <https://docs.github.com/en/get-started/getting-started-with-git/managing-remote-repositories>
- <https://docs.github.com/en/authentication/troubleshooting-ssh/using-ssh-over-the-https-port>
- <https://about.gitlab.com/blog/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/>
