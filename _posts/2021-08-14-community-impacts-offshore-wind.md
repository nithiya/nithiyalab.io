---
title: Mapping the impacts of a proposed offshore wind development plan on Isle of Lewis communities
excerpt: "A poster summarising my MSc in GIS project"
tags:
  - gis
  - python
  - research
  - poster
  - community energy
  - participatory gis
  - offshore wind
  - scotland
  - viewshed
  - multi-criteria analysis
last_modified_at: 2023-09-14
toc: false
---

Streethran, N. (2021) ‘Mapping the impacts of a proposed offshore wind development plan on Isle of Lewis communities’. Aberdeen, United Kingdom, 13 August. DOI: [10.5281/zenodo.6847982](https://doi.org/10.5281/zenodo.6847982).

If you have any feedback, I'd love to hear them!

See the GitHub repository for this project: <https://github.com/nmstreethran/community-energy>

This project was completed at the University of Aberdeen between May and August 2021 in collaboration with Community Energy Scotland.

---

**Abstract:**

This research project investigated the potential impacts of the Sectoral Marine
Plan for Offshore Wind Energy option N4 on island communities. Site N4 is
situated adjacent the northern shore of the Isle of Lewis, with the closest
edge of the plan's area being less than 5 km from the shoreline. As a result,
adverse landscape and seascape changes are expected, which is expected to
affect the local communities, who rely on small-scale economic activities, land
ownership, and tourism as sources of income. To quantify these impacts, this
project used a GIS-based methodology, including a viewshed analysis to assess
visual impacts, multi-criteria analysis to assess the site's suitability for a
commercial-scale offshore wind farm, and a participatory GIS session with
community landowners to gain local knowledge and opinions on the large-scale
development proposal. The conclusions of this project demonstrated the
importance of considering different development scenarios and anticipated
impacts, as well as the usefulness of visually and verbally communicating this
knowledge to communities that may be affected by large-scale offshore wind
development.

**Keywords:** offshore wind energy, multi-criteria analysis, viewshed analysis, participatory GIS, community impacts
