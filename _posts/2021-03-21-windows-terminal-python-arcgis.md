---
title: Setting up Windows Terminal to launch an ArcGIS Pro Python prompt
excerpt: ""
tags:
  - arcgis
  - python
  - gis
  - conda
  - windows
last_modified_at: 2023-10-03
comments: true
---

<div class="notice--success" markdown=1>
{% include fontawesome class="fa-solid fa-circle-info" %} **Related** <br>
Esri Community threads about this topic:
- <https://community.esri.com/t5/arcgis-pro-ideas/enhance-isolated-conda-installation-to-support/idi-p/1178444>
- <https://community.esri.com/t5/arcgis-pro-questions/conda-activate-issues-in-vs-code-using-an-arcpro-3/td-p/1326553>
</div>

If you use [ArcGIS Pro](https://www.esri.com/en-us/arcgis/products/arcgis-pro/overview) for education or work, you will at some point be required to use [Python](https://www.python.org/) scripts to automate some processes without the need to launch the programme's graphical interface. ArcGIS Pro has a [Conda](https://docs.conda.io/en/latest/) environment with all Python requirements which can be cloned and modified as needed.

I generally prefer using [Windows Terminal](https://github.com/Microsoft/Terminal) to launch command prompts instead of launching a separate window for a Python executable. Here's how I set up Windows Terminal to launch my cloned ArcGIS Pro Python environment.

## Prerequisites

Make sure you have the following installed:

- ArcGIS Pro
- either [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
- [Windows Terminal](https://github.com/Microsoft/Terminal#installing-and-running-windows-terminal)

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
See the [Conda user guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html#anaconda-or-miniconda) to determine whether you should install Anaconda or Miniconda. In the following examples, I am using a *single-user installation* of Miniconda. Just replace `miniconda3` with `anaconda3` if you are using Anaconda instead. The Conda paths should also be changed accordingly if a system-wide installation is used.

## Cloning the ArcGIS Pro environment

This can be done either using the graphical interface or the command line.

### Using the graphical interface

Launch ArcGIS Pro without a template, click on the 'Project' menu, click on 'Python', then click 'Manage Environments'. In the window that opens, click on either 'Clone Default' or the 'Clone' icon of the default environment, called `arcgispro-py3`. Selecting the latter will allow you to set a custom path for the cloned environment. Once the environment has been cloned successfully, activate it and close ArcGIS Pro.

### Using the command line

Launch the Python Command Prompt which is situated within the ArcGIS folder in the Start menu (use the search function to find it). If you can't find it, launch it with the following command (which is essentially the target of the Python Command Prompt):

```bat
cmd.exe /k "%USERPROFILE%\\AppData\\Local\\Programs\\ArcGIS\\Pro\\bin\\Python\\Scripts\\proenv.bat"
```

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
The command above assumes a *single-user installation* of ArcGIS Pro. If the programme was installed for all users or in a custom path, then the target would be different, and administrator privileges may be required. To get the target of an application shortcut, right-click the file, click 'Properties', go to the 'Shortcut' tab, and copy the value of 'Target'.

To clone the default ArcGIS Pro environment, run the following in the prompt[^1] [^2]:

```sh
conda create --clone arcgispro-py3 --name ${path_to_env}
```

In the command above, either specify a full custom path (e.g. `G:\Documents\ArcGIS\envs\arcgispro-py3-clone`) or just the name of the environment (e.g. `arcgispro-py3-clone`) in place of `${path_to_env}`. If only the name is specified, the environment will be cloned to the default directory, i.e. `%USERPROFILE%\AppData\Local\Programs\ArcGIS\Pro\bin\Python\envs`.

## Adding the prompt to Windows Terminal

Now, launch Windows Terminal, click on the dropdown arrow, and click on 'Settings'. `settings.json` will now open in your text editor.

In this file, there will be a list of unique profiles for the terminal, the defaults being Windows PowerShell, Command Prompt, and Azure Cloud Shell. I added my ArcGIS Pro Conda prompt at the end of the list, and it looks like this (see Microsoft Docs[^3] for more information):

```json
{
  "guid": "{e1e3667b-a5a6-45ad-8ea2-9409be7ba466}",
  "name": "ArcGIS Pro",
  "commandline": "powershell.exe -ExecutionPolicy ByPass -NoExit -Command \"& '%USERPROFILE%\\miniconda3\\shell\\condabin\\conda-hook.ps1' ; conda activate '${path_to_env}'\"",
  "startingDirectory": "${starting_directory}",
  "icon": "${path_to_icon}"
}
```

The three main keys required are `guid`, `name`, and `commandline`.

A unique GUID must be assigned to each new item in the list. To generate a unique one for a new profile, run `[guid]::NewGuid()` in PowerShell[^4]. A descriptive name should also be assigned in the `name` key (I chose ArcGIS Pro).

Now, the `commandline` value needs to be added. I'm going to use PowerShell, so the value should be of the following format:

```powershell
powershell.exe -ExecutionPolicy ByPass -NoExit -Command \"& '%USERPROFILE%\\miniconda3\\shell\\condabin\\conda-hook.ps1' ; conda activate '${path_to_env}'\"
```

I came up with this command based on the target set for the Anaconda PowerShell Prompt shortcut (which can be found in the `Anaconda3` Start menu folder). The slashes in the paths, as well as double quotes within the command line, must be escaped as shown above.

Optionally, set the starting directory to your usual ArcGIS working directory in place of `${starting_directory}`, and an icon for the terminal profile in place of `${path_to_icon}`.

After saving `settings.json`, the prompt will appear in Windows Terminal's dropdown menu.

## Footnotes

[^1]: [How To: Clone a Python environment with the Python Command Prompt - Esri Technical Support](https://support.esri.com/en-us/knowledge-base/how-to-clone-a-python-environment-with-the-python-comma-000020560)
[^2]: [Cloning an environment - Managing environments - conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#cloning-an-environment)
[^3]: [General profile settings in Windows Terminal - Microsoft Docs](https://learn.microsoft.com/en-us/windows/terminal/customize-settings/profile-general)
[^4]: [Unique identifier - Advanced profile settings in Windows Terminal - Microsoft Docs](https://learn.microsoft.com/en-us/windows/terminal/customize-settings/profile-advanced#unique-identifier)
