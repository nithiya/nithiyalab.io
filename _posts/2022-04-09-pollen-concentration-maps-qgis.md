---
title: Mapping pollen concentration across Great Britain using QGIS
excerpt: ""
tags:
  - gis
  - python
  - visualisation
  - maps
  - qgis
last_modified_at: 2022-06-29
g1:
  - url: /assets/images/pollen-concentration-maps-qgis/PollenMay.png
    image_path: /assets/images/pollen-concentration-maps-qgis/PollenMay.png
    alt: "Pollen concentration in May."
    title: "Pollen concentration in May."
  - url: /assets/images/pollen-concentration-maps-qgis/PollenJune.png
    image_path: /assets/images/pollen-concentration-maps-qgis/PollenJune.png
    alt: "Pollen concentration in June."
    title: "Pollen concentration in June."
  - url: /assets/images/pollen-concentration-maps-qgis/PollenJuly.png
    image_path: /assets/images/pollen-concentration-maps-qgis/PollenJuly.png
    alt: "Pollen concentration in July."
    title: "Pollen concentration in July."
  - url: /assets/images/pollen-concentration-maps-qgis/PollenAugust.png
    image_path: /assets/images/pollen-concentration-maps-qgis/PollenAugust.png
    alt: "Pollen concentration in August."
    title: "Pollen concentration in August."
  - url: /assets/images/pollen-concentration-maps-qgis/PollenSeptember.png
    image_path: /assets/images/pollen-concentration-maps-qgis/PollenSeptember.png
    alt: "Pollen concentration in September."
    title: "Pollen concentration in September."
g2:
  - url: /assets/images/pollen-concentration-maps-qgis/styling1.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling1.png
    alt: "Point styling 1."
    title: "Point styling 1."
  - url: /assets/images/pollen-concentration-maps-qgis/styling2.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling2.png
    alt: "Point styling 2."
    title: "Point styling 2."
  - url: /assets/images/pollen-concentration-maps-qgis/styling3.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling3.png
    alt: "Point styling 3."
    title: "Point styling 3."
g3:
  - url: /assets/images/pollen-concentration-maps-qgis/styling4.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling4.png
    alt: "Raster styling 1."
    title: "Raster styling 1."
  - url: /assets/images/pollen-concentration-maps-qgis/styling5.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling5.png
    alt: "Raster styling 2."
    title: "Raster styling 2."
  - url: /assets/images/pollen-concentration-maps-qgis/styling6.png
    image_path: /assets/images/pollen-concentration-maps-qgis/styling6.png
    alt: "Raster styling 3."
    title: "Raster styling 3."
g4:
  - url: /assets/images/pollen-concentration-maps-qgis/mako.png
    image_path: /assets/images/pollen-concentration-maps-qgis/mako.png
    alt: "Mako (inverted)."
    title: "Mako (inverted)."
  - url: /assets/images/pollen-concentration-maps-qgis/piyg.png
    image_path: /assets/images/pollen-concentration-maps-qgis/piyg.png
    alt: "PiYG (inverted)."
    title: "PiYG (inverted)."
  - url: /assets/images/pollen-concentration-maps-qgis/ylgn.png
    image_path: /assets/images/pollen-concentration-maps-qgis/ylgn.png
    alt: "YlGn."
    title: "YlGn."
  - url: /assets/images/pollen-concentration-maps-qgis/spectral.png
    image_path: /assets/images/pollen-concentration-maps-qgis/spectral.png
    alt: "Spectral (inverted)."
    title: "Spectral (inverted)."
  - url: /assets/images/pollen-concentration-maps-qgis/rocket.png
    image_path: /assets/images/pollen-concentration-maps-qgis/rocket.png
    alt: "Rocket (inverted)."
    title: "Rocket (inverted)."
---

Airborne pollen concentration maps can be a useful indicator of areas to avoid during pollen season for people who suffer from hay fever[^14].

{% include gallery id="g1" caption="Pollen concentration maps." %}

## Tools used

Data (licenced under the [Open Government Licence, version 3.0 (OGLv3.0)](https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/)):

- Abundance of airborne pollen for nine grass species, measured by qPCR, UK, 2016-2017[^1]
- OS Open Zoomstack, layer `names`[^2]
- OS Boundary-Line™, layer `country_region`[^3]

Software:

- [QGIS](https://www.qgis.org/)
  - [GDAL](https://gdal.org/en/latest/)
- [Python 3](https://www.python.org/)
  - [Pandas](https://pandas.pydata.org/)
  - [GeoPandas](https://geopandas.org/en/stable/)

## Data processing

First, the pollen data must be restructured into a GeoPackage using the Python script shown in the section below.

The vector point data is interpolated using the [Inverse Distance Weighted (IDW)](https://en.wikipedia.org/wiki/Inverse_distance_weighting) algorithm in QGIS with the following Python snippet:

```py
layerList = ["May", "Jun", "Jul", "Aug", "Sep"]
extent = (
    "5512.998000000,655989.000000000,5333.809600000,1220310.000000000" +
    " [EPSG:27700]"
)

for layer in layerList:
    interpolation_data = (
        "data/pollen-data.gpkg|layername=" + layer + "::~::0::~::16::~::0"
    )
    params = {
        "INTERPOLATION_DATA": interpolation_data,
        "EXTENT": extent,
        "PIXEL_SIZE": 100,
        "OUTPUT": "data/idw_" + layer + ".tif"
    }
    processing.run("qgis:idwinterpolation", params)
```

Each interpolated raster layer is then clipped to Great Britain's boundary:

```py
for layer in layerList:
    params = {
        "INPUT": "data/idw_" + layer + ".tif",
        "MASK": "data/bdline_gb.gpkg|layername=country_region",
        "SOURCE_CRS": QgsCoordinateReferenceSystem("EPSG:27700"),
        "TARGET_CRS": QgsCoordinateReferenceSystem("EPSG:27700"),
        "OUTPUT": "data/idw_" + layer + "_clipped.tif"
    }

    processing.run("gdal:cliprasterbymasklayer", params)
```

## Mapping procedure

Pollen station point styling:

- Use a rule-based labelling to exclude Belfast using the filter `Site!='Belfast'`
- Use the 'effect drop shadow' symbol available in QGIS' 'Showcase' point symbol list
- Change the fill colour to `#fdbf6f` and outline to `#ff7f00`

{% include gallery id="g2" caption="Point styling." %}

Basemap styling:

- Use OS stylesheets for the appropriate layers[^4] [^5]
- The stylesheets can be applied using the following process:

  ```py
  # Open Zoomstack
  params = {
      "INPUT": "data/OS_Open_Zoomstack.gpkg|layername=names",
      "STYLE": "data/names.qml"
  }
  processing.run("native:setlayerstyle", params)

  # Boundary-Line
  params = {
      "INPUT": "data/bdline_gb.gpkg|layername=country_region",
      "STYLE": "data/country_region.qml"
  }
  processing.run("native:setlayerstyle", params)
  ```

Raster layer styling:

- Singleband pseudocolour
- Discrete interpolation
- Label precision of 1
- Quantile classification mode
- 10 classes
- Cubic resampling

{% include gallery id="g3" caption="Raster layer styling." %}

Raster colour ramps:

{% include gallery id="g4" caption="Raster colour ramps." %}

Mako and Rocket are Seaborn colour maps[^11] [^12], while the rest are from ColorBrewer[^13].

## Python script to restructure pollen data

{% highlight py linenos %}
# import libraries
import pandas as pd
import geopandas as gpd

# read data
pollen = pd.read_csv("data/qPCR_copy_number_abundance_data_aerial_DNA.csv")

# create well known text from coordinates
pollen["wkt"] = (
    "POINT (" + pollen["Long"].astype(str) + " " +
    pollen["Lat"].astype(str) + ")"
)

# drop cells with no value
pollen = pollen.dropna(subset=["Lat", "Long", "MaxPoaceaeConc", "year-month"])

# use full pollen monitoring site name
pollen = pollen.replace({
    "EXE": "Exeter", "EastR": "East Riding", "ESK": "Eskdalemuir",
    "LEIC": "Leicester", "CAR": "Cardiff", "IOW": "Isle of Wight",
    "IPS": "Ipswich", "BNG": "Bangor", "WOR": "Worcester",
    "KCL": "King's College London", "YORK": "York", "ING": "Invergowrie",
    "BEL": "Belfast"
})

# create a geo data frame
pollen = gpd.GeoDataFrame(
    pollen, geometry=gpd.GeoSeries.from_wkt(pollen["wkt"]), crs="EPSG:4326"
)

# drop unnecessary columns
pollen = pollen.drop(columns=["Lat", "Long", "wkt"])

# reproject to BNG
pollen = pollen.to_crs("epsg:27700")

# get list of months with available data
monthList = list(pollen["year-month"].str[0:3].unique())

# get list of pollen monitoring sites
pollen_sites = pollen[["Site", "geometry"]].drop_duplicates()

# aggregate monthly data for each site and save as GeoPackage layers
for m in monthList:
    pollen_month = pollen[pollen["year-month"].str.contains(m)]
    pollen_month = pollen_month.groupby(["Site"]).mean("MaxPoaceaeConc")
    pollen_month = pd.merge(pollen_month, pollen_sites, on="Site")
    pollen_month.to_file("data/pollen-data.gpkg", layer=m)
{% endhighlight %}

## Footnotes

[^1]: Brennan, G., S. Creer, and G. Griffith. 2020. [*Abundance of Airborne Pollen for Nine Grass Species, Measured by qPCR, UK, 2016-2017 [Dataset] [CSV].*](https://doi.org/10.5285/28208be4-0163-45e6-912c-2db205126925) NERC Environmental Information Data Centre.
[^2]: Ordnance Survey (GB). 2021. [*OS Open Zoomstack [Dataset] [GeoPackage] [v2021-12].*](https://www.ordnancesurvey.co.uk/products/os-open-zoomstack).
[^3]: Ordnance Survey (GB). 2021. [*OS Boundary-Line™ [Dataset] [GeoPackage] [v2021-10].*](https://www.ordnancesurvey.co.uk/products/boundary-line)
[^4]: Ordnance Survey (GB). 2021. [*OrdnanceSurvey/OS-Open-Zoomstack-Stylesheets/GeoPackage/QGIS Stylesheets (QML)/Outdoor style/names.qml.*](https://github.com/OrdnanceSurvey/OS-Open-Zoomstack-Stylesheets/tree/master/GeoPackage). GitHub.
[^5]: Ordnance Survey (GB). 2021. [*OrdnanceSurvey/Boundary-Line-stylesheets/Geopackage stylesheets/QGIS Stylesheets (QML)/country_region.qml.*](https://github.com/OrdnanceSurvey/OS-Open-Zoomstack-Stylesheets/tree/master/GeoPackage) GitHub.
[^11]: Rudis, B., N. Ross, and S. Garnier. 2021. [*Introduction to the viridis color maps.*](https://cran.r-project.org/web/packages/viridis/vignettes/intro-to-viridis.html#the-color-scales)
[^12]: Waskom, M. 2021. [*Choosing color palettes - seaborn 0.11.2 documentation.*](https://seaborn.pydata.org/tutorial/color_palettes.html) seaborn.
[^13]: Brewer, C. and M. Harrower. 2021. [*ColorBrewer: Color Advice for Maps.*](https://colorbrewer2.org/) The Pennsylvania State University.
[^14]: Met Office. 2022. [*Pollen Allergies.*](https://www.metoffice.gov.uk/weather/warnings-and-advice/seasonal-advice/health-wellbeing/pollen/pollen-allergies)
