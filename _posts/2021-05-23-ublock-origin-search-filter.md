---
title: Setting up custom uBlock Origin filters to block search results
excerpt: ""
tags:
  - web browsing
  - ublock origin
  - content filtering
  - git
last_modified_at: 2024-09-24
toc: false
comments: true
---

{: .notice--success}
{% include fontawesome class="fa-solid fa-circle-check" %} **Tip** \
Also check out [quenhus/uBlock-Origin-dev-filter on GitHub](https://github.com/quenhus/uBlock-Origin-dev-filter) and [iorate/ublacklist](https://github.com/iorate/ublacklist). These options may be better as these filters can be difficult to maintain in the long run.

Lately, I've noticed a lot of spam and [content farms](https://en.wikipedia.org/wiki/Content_farm) showing up in my web search results. Based on Reddit comments[^1] [^2] on the [uBlock Origin subreddit](https://github.com/gorhill/uBlock), I managed to set up custom filters to prevent these results from showing up on the search page. However, my filter list was getting longer and thus more difficult to maintain. So, I decided to use a few tools to automate the process.

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
According to its [wiki](https://github.com/gorhill/uBlock/wiki/Static-filter-syntax), uBlockOrigin "supports most of [EasyList](https://easylist.to/) filter syntax", so the [Adblock Plus](https://help.adblockplus.org/hc/en-us/articles/360062733293-How-to-write-filters) and [AdGuard](https://adguard.com/kb/general/ad-filtering/create-own-filters/) filter syntax documentation are useful references.

1.  Create a private GitHub Gist and clone it to your local machine. The `${gistid}` can be found in the Gist's URL, i.e. `https://gist.github.com/${username}/${gistid}`.

    Clone via HTTPS:

    ```sh
    git clone https://gist.github.com/${gistid}.git
    ```

    Clone via SSH:

    ```sh
    git clone git@gist.github.com:${gistid}.git
    ```

2.  Navigate to the cloned Gist's directory. Create a new text file (I called mine `searchfilter.txt`) and list the domains that you want to block in your search results. Each unique domain should occupy a new line. For example:

    ```text
    example.com
    .example.net
    www.example.xyz
    ```

    {: .notice--info}
    {% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
    `.example.net` will block domains with prefixes like `www.example.net` and `docs.example.net`.

3.  In the same directory, create a new text file to store [`awk`](https://en.wikipedia.org/wiki/AWK) scripts that will convert these domains into uBlock Origin filters. The example script below, which I've named `searchfilter.sh`, shows the `awk` commands for creating DuckDuckGo, Ecosia, and Google filters.

    ```sh
    #!/bin/sh

    # DuckDuckGo
    echo "! Title: Custom filters - DuckDuckGo search results
    " > ddg.txt
    awk '{
    print "\
    duckduckgo.com##a[data-testid=\"result-title-a\"][href*=\""$1"\"]\
    :upward(.nrn-react-div)\n\
    duckduckgo.com##.tile-wrap a[href*=\""$1"\"]:upward(.tile)"
    }' searchfilter.txt >> ddg.txt

    # Ecosia
    echo "! Title: Custom filters - Ecosia search results
    " > ecosia.txt
    awk '{
    print "ecosia.org##a[href*=\""$1"\"]:upward(2)"
    }' searchfilter.txt >> ecosia.txt

    # Google
    echo "! Title: Custom filters - Google search results
    " > google.txt
    awk '{
    print "google.*##a[href*=\""$1"\"]:upward(2)"
    }' searchfilter.txt >> google.txt
    ```

4.  Run the `awk` script in the terminal within the same directory (note that you'll need to run it using an environment like [Git Bash](https://gitforwindows.org/), [Cygwin](https://www.cygwin.com/), or [MSYS](https://www.msys2.org/) on Windows):

    ```sh
    ./searchfilter.sh
    ```

    This will create a new text file for each search engine you have defined in the script.

5.  Commit and push the changes to the remote Gist.

6.  Open your browser's uBlock Origin dashboard. Click on the 'Filter lists' tab and scroll down to 'Custom'. Check the 'Import' box and add the filters' GitHub Gist URLs, which will be of the following form (replace `${filename}` with `ddg.txt`, `ecosia.txt`, or `google.txt`):

    ```text
    https://gist.github.com/${username}/${gistid}/raw/${filename}
    ```

    Once added, click 'Apply changes', and your filters should be active right away.

    If you make any changes to the list, just run the `awk` script again, commit, and push to the remote as usual. Force update your custom filters in the uBlock Origin dashboard for the updated filters to take effect immediately.

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
See uBlock Origin's wiki on [Reddit](https://www.reddit.com/r/uBlockOrigin/wiki/solutions) and [GitHub](https://github.com/gorhill/uBlock/wiki/Static-filter-syntax) for more information about creating and applying filters.

## Footnotes

[^1]: [How to create an "inverse" filter, "block everything but"? - /r/uBlockOrigin/ - Reddit](https://redd.it/v06ovk)
[^2]: [Can I use wildcards to block google search results for all country domains of a site - /r/uBlockOrigin/ - Reddit](https://redd.it/izihll)
