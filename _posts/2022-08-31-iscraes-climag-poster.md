---
title: Increased severity and frequency of fodder production deficits under future climate conditions in Ireland
excerpt: "A poster presented at the International Symposium on Climate-Resilient Agri-Environmental Systems in Dublin."
tags:
  - research
  - poster
  - ireland
  - agriculture
  - fodder crises
  - climate change
  - pasture systems
last_modified_at: 2024-08-19
toc: false
g1:
  - url: /assets/images/iscraes-climag-poster/tweet.png
    image_path: /assets/images/iscraes-climag-poster/tweet.png
    alt: "ClimAg poster screenshot."
    title: "ClimAg poster screenshot."
---

Leahy, P., Streethran, N., Hickey, K., and Wingler, A. (2022) ‘Increased severity and frequency of fodder production deficits under future climate conditions in Ireland’. International Symposium on Climate-Resilient Agri-Environmental Systems, Dublin, Ireland, 29 August. Available at: <https://www.iscraes.org/wp-content/uploads/2023/01/Paul-Leahy-Poster-grassland-poster-ISCRAES-2022.pdf> (Accessed: 27 March 2023).

ResearchGate: <https://www.researchgate.net/publication/364330814>

Tweet [ClimAtt_Project/status/1564666240945324034]:

{% include gallery id="g1" caption="ClimAg poster screenshot." %}

If you have any feedback, I'd love to hear them!

See the ClimAg project's GitHub organisation for updates: <https://github.com/ClimAg>

---

**Abstract as published in the Book of Abstracts:**

Irish agriculture is heavily centred on dairy and livestock farming, with the vast majority of the
agricultural area used for grass production or rough grazing. The temperate and humid climate of
Ireland is well-suited to grass production. Animals spend most of the year outdoors and grass
silage is widely used as fodder. However, in recent years, several “fodder crises” have been
reported, where fodder supplies run low resulting in threats to farms’ economic performance and
productivity and to animal health. Fodder crises are multifactorial in nature, but weather conditions
at the seasonal or multi-annual scale are a significant driver. For example, severe shortages of
fodder occurred in summer 2018, when warm and dry conditions between May and mid-July led
to significant soil moisture deficits by mid-June, with deficits in the southeast of the country
reaching over 95 mm. The total rainfall at the Enniscorthy station during the months of May, June
and July was only 37% of the long-term average. This study uses simulated rainfall data from
CMIP5 models (NorESM1-M model, RCP8.5 scenario) to examine changes in May-June-July
rainfall between past (1981-2010) and mid- 21st century (2041-2070) climate change scenarios.
The results show that events similar to that of 2018 will increase in frequency and severity by the
mid-21st century. Average 2041-2070 May-June-July rainfall will decrease by c. 13% relative to
1981-2010. Rainfall values similar to those of the 2018 fodder crisis will become approximately
twice as likely by 2041-2070. Grass-based agricultural systems in Ireland are therefore likely to
become more frequently stressed in the future, and changes in management will be required in
order to improve resilience to disruptions from extreme weather conditions.

**Keywords:** fodder crises; resilient agriculture; climate change; grassland systems.
