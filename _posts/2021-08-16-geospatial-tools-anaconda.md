---
title: Using various geospatial tools with Anaconda
excerpt: ""
tags:
  - gis
  - python
  - r
  - conda
  - package management
last_modified_at: 2024-03-19
---

Install [Anaconda](https://www.anaconda.com/) if it is not already available on your system (I recommend the lightweight [Miniconda](https://docs.conda.io/en/latest/miniconda.html)). On Arch Linux, it can be installed from the AUR:

```sh
yay -Syu miniconda3
```

To set up an Anaconda virtual environment with the required tools (QGIS, Python, or R), save the snippet under the relevant section as a YAML file (e.g. `environment.yml`). Then, run the following commands, replacing `${envname}` with the name of the environment as defined in the first line of the YAML file:

```sh
conda env create --file environment.yml
conda activate ${envname}
```

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
All of these packages can be installed in a single Anaconda virtual environment via the [conda-forge](https://conda-forge.org/) channel (though I haven't checked to see if there are potential conflicts).

## QGIS

```yml
name: qgis
channels:
  - conda-forge
  - defaults
dependencies:
  - python=3
  - qgis
```

## Python

```yml
name: py-geo
channels:
  - conda-forge
  - anaconda
dependencies:
  - python=3
  - geopandas
  - rioxarray
  - rasterstats
  - scipy
  - cfgrib
  - netcdf4
  - cdo  # unavailable on Windows; use Conda with WSL
  - dask  # parallel computing
  - pooch  # to download data
  - matplotlib  # plotting tools
  - matplotlib_scalebar
  - nc-time-axis
  - seaborn
  - cartopy
  - mapclassify
  - contextily
  - pyviz
  - hvplot
  - statsmodels
  - jupyterlab  # interactive notebooks
  - jupyter_bokeh
  - pytest  # testing and linting
  - flake8
  - pylint
  - black
  - black-jupyter
```

## R

See [this post]({{ "post/r-setup/#r-with-jupyterlab-via-anaconda" | relative_url }}) for more information about setting up R to use with [JupyterLab](https://jupyter.org/) in an Anaconda environment.

```yml
name: r-geo
channels:
  - conda-forge
  - r
  - anaconda
dependencies:
  - r-base
  - r-essentials
  - libgit2  # to install packages from Git repositories
  - r-devtools
  - r-terra
  - r-rgdal
  - r-sf
  - r-geojsonio
  - r-rgeos
  - r-rastervis
  - jupyterlab
```
