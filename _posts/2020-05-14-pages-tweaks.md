---
title: "My Minimal Mistakes Jekyll pages tweaks"
excerpt: ""
tags:
  - gitlab pages
  - jekyll
  - website
  - git
last_modified_at: 2024-05-05
header:
  actions:
    - label: "<i class='fa-brands fa-gitlab'></i> View on GitLab"
      url: https://gitlab.com/nithiya/nithiya.gitlab.io
redirect_from:
  - /development/pages-tweaks/
comments: true
---

This website uses the [Minimal Mistakes](https://mademistakes.com/work/jekyll-themes/minimal-mistakes/) theme.
This theme is highly configurable, so I modified it to my liking and documented my changes here. 😄

To serve this site locally:

1.  Install [Ruby](https://www.ruby-lang.org/en/).

2.  Clone [this repository](https://gitlab.com/nithiya/nithiya.gitlab.io).

    Option 1 - SSH:

    ```sh
    git clone git@gitlab.com:nithiya/nithiya.gitlab.io.git
    ```

    Option 2 - HTTPS:

    ```sh
    git clone https://gitlab.com/nithiya/nithiya.gitlab.io.git
    ```

3.  Navigate to the cloned directory and install all requirements:

    ```sh
    cd nithiya.gitlab.io
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000`.

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Note** \
The following configurations work best with v4.24.0 of the Minimal Mistakes theme. If you get errors while trying to `bundle install`, some prerequisite libraries, such as `make` may be missing. Check the error messages and install the missing libraries to fix this issue. To update gems, use `bundle update`.

## Theme configurations

Firstly, I configured the theme and skin in `_config.yml`:

```yml
theme: minimal-mistakes-jekyll
minimal_mistakes_skin: dark
```

I also configured the locale and date format:

```yml
locale: en-GB
date_format: "%-d %B %Y"
```

## Fonts

I'm self-hosting fonts used on this site.

I configured the following fonts and set the caption font family in `assets/css/main.scss`:

```scss
$sans-serif: "FiraGO", "Fira Sans", "Source Sans 3", -apple-system,
  BlinkMacSystemFont, "Roboto", "Segoe UI", "Helvetica Neue", "Lucida Grande",
  Arial, sans-serif !default;
$monospace: "Fira Code", "Fira Mono", "Source Code Pro", Monaco,
  "Cascadia Code", "Cascadia Mono", Consolas, "Lucida Console",
  monospace !default;
$caption-font-family: $sans-serif !default;
```

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
The fonts I'm using are: [**FiraGO** v1.001](https://github.com/bBoxType/FiraGO) (body --- regular, italic, bold, and bold italic), [**Fira Code** v5.2](https://github.com/tonsky/FiraCode) (monospace --- regular and bold), and [**Homemade Apple**](https://fonts.google.com/specimen/Homemade+Apple) (site title).

For use on this website, a subset of the font styles were created and converted into WOFF2 format from TTF using [fontTools](https://github.com/fonttools/fonttools). fontTools is a Python package which requires [Brotli](https://github.com/google/brotli) to convert fonts to WOFF2. The requirements are installed using pip:

```sh
python -m pip install fonttools brotli  # using Linux
py -m pip install fonttools brotli  # using Windows
```

The generated files can be found in `assets/fonts`. The font faces were then specified in `assets/css/main.scss`. For example:

```scss
@font-face {
  font-family: "FiraGO";
  src: url("../fonts/FiraGO/FiraGO-Italic.woff2") format("woff2");
  font-weight: 400;
  font-style: italic;
}
```

I also set the site title font as follows:

```scss
.site-title {
  font-family: "Homemade Apple", cursive;
  font-weight: 400;
  font-style: normal;
}
```

The following commands, for example, were used to generate the WOFF2 font files from TTFs of Fira Code Regular and Homemade Apple; the required glyphs are specified using Unicode characters and/or text[^1]:

```sh
pyftsubset FiraCode-Regular.ttf --unicodes=U+0020-007E --flavor=woff2 --output-file=FiraCode-Regular.woff2
pyftsubset HomemadeApple-Regular.ttf --text="Nithiya Streethran" --flavor=woff2 --output-file=HomemadeApple-Regular.woff2
```

To italicise comments in code blocks, I added the following to `assets/css/main.scss`:

```scss
.highlight {
  .c, .c1 {
      font-style: italic;
  }
}
```

## Font Awesome icons

To add [Font Awesome](https://fontawesome.com) icons to Markdown files, I created the helper `_includes/fontawesome`, which contains the following:

```html
{% raw %}<i class="{{ include.class }}" aria-hidden="true"></i>{% endraw %}
```

To display an icon, I simply add the following, replacing `fa-solid fa-envelope` with the icon's class:

```md
{% raw %}{% include fontawesome class="fa-solid fa-envelope" %}{% endraw %}
```

## Footer

In `_includes/footer.html`, I changed some of the icons used and also added custom copyright information:

{% highlight html linenos %}
{% raw %}<div class="page__footer-follow">
  <ul class="social-icons">
    {% if site.footer.links %}
      {% for link in site.footer.links %}
        {% if link.label and link.url %}
          <li><a href="{{ link.url }}" rel="nofollow noopener noreferrer"><i class="{{ link.icon | default: 'fa-solid fa-link' }}" aria-hidden="true"></i> {{ link.label }}</a></li>
        {% endif %}
      {% endfor %}
    {% endif %}

    {% unless site.atom_feed.hide %}
      <li><a href="{% if site.atom_feed.path %}{{ site.atom_feed.path }}{% else %}{{ '/feed.xml' | relative_url }}{% endif %}"><i class="fa-solid fa-rss" aria-hidden="true"></i> {{ site.data.ui-text[site.locale].feed_label | default: "Feed" }}</a></li>
    {% endunless %}
  </ul>
</div>

<div class="page__footer-copyright">
  &copy; {{ site.time | date: '%Y' }} <a href="{{ site.copyright_url | default: site.url }}">{{ site.copyright | default: site.title }}</a>. {{ site.data.ui-text[site.locale].powered_by | default: "Powered by" }} <a href="https://jekyllrb.com" rel="nofollow">Jekyll</a> &amp; <a href="https://mademistakes.com/work/jekyll-themes/minimal-mistakes/" rel="nofollow">Minimal Mistakes</a>.
  <br/>
  <strong><i class="fa-brands fa-creative-commons fa-lg" aria-hidden="true"></i> <i class="fa-brands fa-creative-commons-by fa-lg" aria-hidden="true"></i></strong> Except where otherwise noted, content on this site is licensed under a <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International (CC-BY-4.0) License</a>.
</div>{% endraw %}
{% endhighlight %}

## Colours

In `assets/css/main.scss`, I used the [Woodland base16 colour scheme](https://github.com/jcornwall/base16-woodland-scheme) to override the `dark` skin's default colours:

```scss
$base00: #231e18 !default;
$base01: #302b25 !default;
$base02: #48413a !default;
$base03: #9d8b70 !default;
$base04: #b4a490 !default;
$base05: #cabcb1 !default;
$base06: #d7c8bc !default;
$base07: #e4d4c8 !default;
$base08: #d35c5c !default;
$base09: #ca7f32 !default;
$base0a: #e0ac16 !default;
$base0b: #b7ba53 !default;
$base0c: #6eb958 !default;
$base0d: #88a4d3 !default;
$base0e: #bb90e2 !default;
$base0f: #b49368 !default;
```

I then assigned these colours:

```scss
$primary-color: $base0b !default;
$link-color: $base0c !default;
$background-color: $base01 !default;
```

I then made a minor fix to the colour of the active link hover in the table of contents, so it stands out from the background. I added the following to `assets/css/main.scss`:

```scss
.toc .active a:hover {
  color: inherit;
}
```

## Notices

To change the appearance of notices, I added the following to `assets/css/main.scss`:

```scss
@mixin notice($notice-color) {
  color: inherit;
  background-color: $notice-color;
  a {
    color: inherit;
  };
  code {
    background-color: mix($background-color, $notice-color, 25%);
  };
}

.notice {
  @include notice($base0f);
}

.notice--primary {
  @include notice($primary-color);
}

.notice--info {
  @include notice($info-color);
}

.notice--warning {
  @include notice($warning-color);
}

.notice--success {
  @include notice($success-color);
}

.notice--danger {
  @include notice($danger-color);
}
```

{: .notice--success}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
See Minimal Mistakes' documentation for more information about [utility classes](https://mmistakes.github.io/minimal-mistakes/docs/utility-classes/), such as notices, [helpers](https://mmistakes.github.io/minimal-mistakes/docs/helpers/), and [stylesheets](https://mmistakes.github.io/minimal-mistakes/docs/stylesheets/).

## Pages

I created `_pages/sitemap.html` based on some of the Minimal Mistakes archive pages:

{% highlight html linenos %}
{% raw %}---
title: Sitemap
layout: archive
permalink: /sitemap/
entries_layout: grid
---

A list of all the posts and pages found on this site. For you robots out there, there is an <a href="{{ "sitemap.xml" | relative_url }}">XML version</a> available for digesting as well.

<!-- Get list of pages, excluding ones without a title -->
{%- capture site_pages -%}
  {%- for post in site.pages -%}
    {%- if post.title -%}
      {{- post | first -}}
      {%- unless forloop.last -%}
        ,
      {%- endunless -%}
    {%- endif -%}
  {%- endfor -%}
{%- endcapture -%}
{%- assign pages_list = site_pages | split: ',' -%}

<!-- Display taxonomy index -->
<ul class="taxonomy__index">
  <li>
    <a href="#posts">
      <strong>Posts</strong> <span class="taxonomy__count">{{- site.posts.size -}}</span>
    </a>
  </li>
  <li>
    <a href="#pages">
      <strong>Pages</strong> <span class="taxonomy__count">{{- pages_list.size -}}</span>
    </a>
  </li>
</ul>

<!-- Assign layout of entries -->
{%- assign entries_layout = page.entries_layout | default: 'list' -%}

<!-- Display list of posts -->
<section id="posts" class="taxonomy__section">
  <h2 class="archive__subtitle">Posts</h2>
  <div class="entries-{{ entries_layout }}">
    {%- for post in site.posts -%}
      {%- include archive-single.html type=entries_layout -%}
    {%- endfor -%}
  </div>
  <a href="#page-title" class="back-to-top">{{- site.data.ui-text[site.locale].back_to_top | default: 'Back to Top' -}} &uarr;</a>
</section>

<!-- Display list of pages -->
<section id="pages" class="taxonomy__section">
  <h2 class="archive__subtitle">Pages</h2>
  <div class="entries-{{ entries_layout }}">
    {%- for post in site.pages -%}
      {%- if post.title -%}
        {%- include archive-single.html type=entries_layout -%}
      {%- endif -%}
    {%- endfor -%}
  </div>
  <a href="#page-title" class="back-to-top">{{- site.data.ui-text[site.locale].back_to_top | default: 'Back to Top' -}} &uarr;</a>
</section>{% endraw %}
{% endhighlight %}

To exclude pages such as `robots.txt` and files in the `assets` directory from the list of pages, I set the Liquid code to capture only pages with a title. This was necessary as setting `sitemap: false` won't work for pages hosted using GitHub or GitLab Pages[^3].

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
See the [Liquid](https://shopify.github.io/liquid/) and [Jekyll](https://jekyllrb.com/docs/liquid/) docs for more information about the Liquid syntax.

I also commented out the `paginate` line in `_config.yml`, which allowed me to use a custom `index.md` file without the `home` layout, instead of the default `index.html` homepage that shows the 5 most recent posts[^2]:

```yml
# paginate: # amount of posts to show
```

## Comments

{: .notice--info}
{% include fontawesome class="fa-solid fa-circle-info" %} **Info** \
Note that comments are currently disabled on this site.

I've set up [Staticman](https://staticman.net/) comments on this site. Check out [this post]({{ "post/staticman-jekyll-gitlab" | relative_url }}) to find out how I did it.

## External link indicator

To automatically add an external link indicator, which uses a Font Awesome icon, I configured the following in `assets/css/main.scss`[^7] [^8] [^9]. This excludes the indicator in the social icons and buttons:

```scss
.page {
  a:not(.btn):not([href*="localhost:4000"]):not([href*="//nithiya.gitlab.io"]):not([href^="#"]):not([href^="/"])::after {
    content: " \f35d";
    font-family: "Font Awesome 6 Free";
    display: inline-block;
    text-rendering: auto;
    font-weight: 900;
    font-variant: normal;
    font-size: small;
    font-style: normal;
    white-space: pre;
    -webkit-font-smoothing: antialiased;
  }
}
```

## Captions

To change the gallery counter's font family, I added the following to `assets/css/main.scss`:

```scss
.mfp-counter {
  font-family: $caption-font-family;
}
```

## Favicon

To change the site's favicon, I used a favicon generator and pasted the generated HTML in `_includes/head/custom.html`. I dropped the generated icons in the root of the repository[^4]. Keeping `favicon.ico` in the root will suppress the `ERROR '/favicon.ico' not found.` message[^5]. The icon I used is `emoji_u1f989` from [Noto Emoji, v2017-05-18-cook-color-fix](https://github.com/googlefonts/noto-emoji/tree/v2017-05-18-cook-color-fix).

## Footnotes

[^1]: [subset - fontTools Documentation](https://fonttools.readthedocs.io/en/latest/subset/index.html)
[^2]: [Paginate - Configuration - Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/docs/configuration/#paginate)
[^3]: [sitemap: false doesn't work - Issue #991 - mmistakes/minimal-mistakes - GitHub](https://github.com/mmistakes/minimal-mistakes/issues/991#issuecomment-369307466)
[^4]: [How do we change the favicon in the title bar? - Issue #585 - mmistakes/minimal-mistakes - GitHub](https://github.com/mmistakes/minimal-mistakes/issues/585#issuecomment-253569318)
[^5]: [ERROR \`/favicon.ico' not found - Issue #289 - mmistakes/minimal-mistakes - GitHub](https://github.com/mmistakes/minimal-mistakes/issues/389#issuecomment-231505632)
[^7]: [Adding External Link Indicator with CSS - Waaz Solutions](https://dangrabbits.dev/adding-external-link-indicator-with-css/)
[^8]: [CSS Pseudo-elements - Font Awesome](https://docs.fontawesome.com/web/add-icons/pseudo-elements)
[^9]: [Add a space (" ") after an element using :after - Stack Overflow](https://stackoverflow.com/a/40963855)
