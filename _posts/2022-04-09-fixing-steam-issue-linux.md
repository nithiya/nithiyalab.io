---
title: Getting Steam to work on a Linux system with hybrid graphics
excerpt: ""
tags:
  - steam
  - linux
last_modified_at: 2022-05-14
toc: false
---

Using Steam on a Linux system with [hybrid graphics](https://wiki.archlinux.org/title/Hybrid_graphics) can cause display issues, such as a blank or unresponsive screen. Thanks to a Reddit post[^1], which pointed out a solution from the Steam Community forums[^2], I managed to resolve these issues.

After installation, copy `/usr/share/applications/steam.desktop` to `~/.local/share/applications/steam.desktop`.

Open the copied desktop file and change the line `PrefersNonDefaultGPU=true` to `PrefersNonDefaultGPU=false`.

When set to false, Steam will use the more powerful GPU instead of the integrated graphics.

Now the Steam client can be launched using the desktop icon without any display issues.

## Footnotes

[^1]: [Editing Steam's .desktop files per this post fixed my problem with Steam displaying solid black windows unless launched from Terminal - Reddit](https://redd.it/tqai5t)
[^2]: [Steam works only if launched from terminal - Steam for Linux General Discussion](https://steamcommunity.com/app/221410/discussions/0/3051736373892813276/?ctp=2#c5034464604293516297)
