# nithiya.gitlab.io

**My personal website!**

## Serving this site locally

1.  Install [Ruby](https://www.ruby-lang.org/en/).

2.  Clone [this repository](https://gitlab.com/nithiya/nithiya.gitlab.io).

    Option 1 - SSH:

    ```sh
    git clone git@gitlab.com:nithiya/nithiya.gitlab.io.git
    ```

    Option 2 - HTTPS:

    ```sh
    git clone https://gitlab.com/nithiya/nithiya.gitlab.io.git
    ```

3.  Navigate to the cloned directory and install all requirements:

    ```sh
    cd nithiya.gitlab.io
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000`.

## Issues

If you get errors while trying to `bundle install`, some prerequisite libraries, such as `make` may be missing. Check the error messages and install the missing libraries to fix this issue.

To update gems:

```sh
bundle update
```

## Licence

Copyright (c) 2019-2024 Nithiya Streethran (nmstreethran at gmail dot com).

This site's source code is available on [GitLab](https://gitlab.com/nithiya/nithiya.gitlab.io) under the terms of the [MIT License](https://opensource.org/license/mit).

Except where otherwise noted, content on this site is licensed under a [Creative Commons Attribution 4.0 International (CC-BY-4.0) License](https://creativecommons.org/licenses/by/4.0/).
Footnotes and/or external links are provided if external content is used.

## Assets

- [Minimal Mistakes](https://mademistakes.com/work/jekyll-themes/minimal-mistakes/) - MIT License
- [Jekyll](https://jekyllrb.com/) - MIT License
- [GitLab](https://about.gitlab.com/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) - [MIT Expat License and other licenses](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)
- [The Noun Project](https://thenounproject.com/) - [Creative Commons Attribution 3.0 United States (CC-BY-3.0-US) License](https://creativecommons.org/licenses/by/3.0/us/)
- [Font Awesome](https://fontawesome.com/) 6 - [SIL Open Font License 1.1 (OFL-1.1)](https://opensource.org/license/ofl-1-1) (fonts), MIT License (code), CC-BY-4.0 (icons)
- [Susy](https://www.oddbird.net/susy/) - [BSD 3-clause "New" or "Revised" License (BSD-3-Clause)](https://opensource.org/license/bsd-3-clause)
- [Breakpoint](https://github.com/at-import/breakpoint) - MIT License or [GNU General Public License, version 2 (GPL-2.0)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
- [FitVids.js](https://github.com/davatron5000/FitVids.js/) - [Do What The F\*\*\* You Want To Public License (WTFPL)](http://www.wtfpl.net/)
- [Magnific Popup](https://dimsemenov.com/plugins/magnific-popup/) - MIT License
- [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll) - MIT License
- [Gumshoejs](https://github.com/cferdinandi/gumshoe) - MIT License
- [jQuery throttle / debounce](https://benalman.com/projects/jquery-throttle-debounce-plugin/) - MIT License
- [GreedyNav.js](https://github.com/lukejacksonn/GreedyNav) - MIT License
- [Jekyll Group-By-Array](https://github.com/mushishi78/jekyll-group-by-array) - MIT License
- [@allejo's Pure Liquid Jekyll Table of Contents](https://allejo.io/blog/a-jekyll-toc-in-liquid-only/) - MIT License
- [Lunr](https://lunrjs.com) - MIT License
- [clipboard.js](https://clipboardjs.com/) - MIT License
- [Woodland base16 colour scheme](https://github.com/jcornwall/base16-woodland-scheme) - MIT License
- [FiraGO](https://github.com/bBoxType/FiraGO), Version 1.001 (20 March 2018) - OFL-1.1
- [Fira Code](https://github.com/tonsky/FiraCode), Version 5.2 (12 June 2020) - OFL-1.1
- [Homemade Apple](https://fonts.google.com/specimen/Homemade+Apple) - [Apache License, Version 2.0 (Apache-2.0)](https://www.apache.org/licenses/LICENSE-2.0)
- [Staticman](https://staticman.net/) - MIT License
- [Fly](https://fly.io/)
- [Akismet](https://akismet.com)
- [reCAPTCHA](https://www.google.com/recaptcha/about/)
- [Noto Emoji, v2017-05-18-cook-color-fix](https://github.com/googlefonts/noto-emoji/tree/v2017-05-18-cook-color-fix) - Apache-2.0
- [awesome_bot](https://github.com/dkhamsing/awesome_bot) - MIT License
- [Shields.io](https://shields.io) - [Creative Commons Zero v1.0 Universal (CC0-1.0) License](https://creativecommons.org/publicdomain/zero/1.0/)

## Privacy

All comments posted will be available publicly on [GitLab](https://gitlab.com/nithiya/nithiya.gitlab.io/-/tree/main/_data/comments).
An email address is required to post comments, but it will not be published.
reCAPTCHA is used to minimise spam.
