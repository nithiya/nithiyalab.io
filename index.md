---
title: Welcome!
last_modified_at: 2024-11-19
---

**Hello there. Welcome to my site!** 😄

I'm an interdisciplinary environmental researcher with a background in energy engineering and geosciences.

I'm interested in using my programming and data analysis skills to support environmental decision-making.

This site presently hosts a random collection of notes but I aim to post more about research related stuff in the near future.

Thanks for stopping by!
