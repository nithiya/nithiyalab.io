# Fonts

This website incorporates the [FiraGO](https://github.com/bBoxType/FiraGO), [Fira Code](https://github.com/tonsky/FiraCode), and [Homemade Apple](https://fonts.google.com/specimen/Homemade+Apple) fonts. For use on this website, a subset of the font styles were created and converted into WOFF2 format from TTF using [fontTools](https://github.com/fonttools/fonttools). fontTools is a Python package which requires [Brotli](https://github.com/google/brotli) to convert fonts to WOFF2. Install the requirements using pip:

```sh
python -m pip install fonttools brotli  # using Linux
py -m pip install fonttools brotli  # using Windows
```

The generated files can be found in `assets/fonts`.

The font faces were then specified in `assets/css/main.scss`.

## FiraGO

Used as the main font.

Version 1.001 (20 March 2018)

- Copyright (c) 2012-2018 Carrois Corporate GbR and HERE Europe B.V. for FiraGO digitised data, with Reserved Font Name "Fira".
- Copyright (c) 2012-2018 The Mozilla Foundation, Telefonica S.A., Carrois Corporate GbR, and bBox Type GmbH for Fira Sans digitised data up to version 4.3, with Reserved Font Name "Fira".
- Copyright (c) 2016-present Carrois Corporate GbR and bBox Type GmbH for Fira Sans & FiraGO designs.

This Font Software is licensed under the [SIL Open Font License, Version 1.1 (OFL-1.1)](https://opensource.org/license/ofl-1-1).

Font weights / styles used:

- Regular (400)
- Italic (400)
- Bold (700)
- Bold Italic (700)

Subsets were created using the following commands:

```sh
pyftsubset FiraGO-Regular.ttf --unicodes=U+0020-007E,00A9,00AE,00B0,00B1,00D7,00E1,00E4,00E5,00E7,0142,2013,2014,2018,2019,201C,201D,2022,2026,2191,21A9 --flavor=woff2 --output-file=FiraGO-Regular.woff2
pyftsubset FiraGO-Bold.ttf --unicodes=U+0020-007E,00A9,00AE,00B0,00B1,00D7,00E1,00E4,00E5,00E7,0142,2013,2014,2018,2019,201C,201D,2022,2026,2191,21A9 --flavor=woff2 --output-file=FiraGO-Bold.woff2
pyftsubset FiraGO-Italic.ttf --unicodes=U+0020-007E,00A9,00AE,00B0,00B1,00D7,00E1,00E4,00E5,00E7,0142,2013,2014,2018,2019,201C,201D,2022,2026,2191,21A9 --flavor=woff2 --output-file=FiraGO-Italic.woff2
pyftsubset FiraGO-BoldItalic.ttf --unicodes=U+0020-007E,00A9,00AE,00B0,00B1,00D7,00E1,00E4,00E5,00E7,0142,2013,2014,2018,2019,201C,201D,2022,2026,2191,21A9 --flavor=woff2 --output-file=FiraGO-BoldItalic.woff2
```

## Fira Code

Used as the monospace font.

Version 5.2 (12 June 2020)

Copyright (c) 2014 The Fira Code Project Authors.

This Font Software is licensed under the OFL-1.1.

Font weights used:

- Regular (400)
- Bold (700)

Subsets were created using the following commands:

```sh
pyftsubset FiraCode-Bold.ttf --unicodes=U+0020-007E --flavor=woff2 --output-file=FiraCode-Bold.woff2
pyftsubset FiraCode-Regular.ttf --unicodes=U+0020-007E --flavor=woff2 --output-file=FiraCode-Regular.woff2
```

## Homemade Apple

Used as the site title's font.

Copyright (c) Font Diner.

This Font Software is licensed under the Apache License, Version 2.0 (Apache-2.0).

Subsets were created using the following command:

```sh
pyftsubset HomemadeApple-Regular.ttf --text="Nithiya Streethran" --flavor=woff2 --output-file=HomemadeApple-Regular.woff2
```

## Unicode reference

U+0020-007E are Basic Latin characters.
<!-- [^\u0020-\u007E] regex basic latin -->

Code | Symbol
-- | --
U+00A9 | ©
U+00AE | ®
U+00B0 | °
U+00B1 | ±
U+00D7 | ×
U+00E1 | á
U+00E4 | ä
U+00E5 | å
U+00E7 | ç
U+0142 | ł
U+2013 | –
U+2014 | —
U+2018 | ‘
U+2019 | ’
U+201C | “
U+201D | ”
U+2022 | •
U+2026 | …
U+2191 | ↑
U+21A9 | ↩

## References

- <https://en.wikipedia.org/wiki/List_of_Unicode_characters>
- <https://fonttools.readthedocs.io/en/latest/subset/index.html>
